#! /bin/bash

ConfDir="/etc/onboard"
ThemeConf="${ConfDir}/glory-theme-defaults.conf"
CurrentConf="${ConfDir}/onboard-defaults.conf"
OldConf="${ConfDir}/old-defaults.conf"


post_install () {
	if [[ -f "${CurrentConf}" ]] && [[ ! -f "${OldConf}" ]]; then
		mv "${CurrentConf}" "${OldConf}"
	fi

	cp "${ThemeConf}" "${CurrentConf}"
}


pre_upgrade () {
	if [[ "$(stat --format=%Y "${ThemeConf}")" -eq "$(stat --format=%Y "${CurrentConf}")" ]]; then
		rm "${CurrentConf}"
	fi
}


post_upgrade () {
	cp --no-clobber "${ThemeConf}" "${CurrentConf}"
}


pre_remove () {
	if [[ -f "${OldConf}" ]]; then
		mv --force "${OldConf}" "${CurrentConf}"
	else
		rm "${CurrentConf}"
	fi
}
